<?php

	// Load up parent theme and make child theme styles dependent on them
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
	function theme_enqueue_styles() {

        // Register Styles
        wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,900', array(), 1.0, 'screen');

        //Enqueue Styles/Scripts
	    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/public/styles.css');
       	wp_enqueue_style('google-fonts');
       	
       	
       	

        // Register Scripts
        if (!is_admin() ) {
		    wp_register_script('child-scripts', get_stylesheet_directory_uri() . '/js/child-scripts.js', false, 1, true);
		    wp_register_script('gallery', get_stylesheet_directory_uri() . '/js/gallery.js', false, 1, true);
		    wp_register_script('skrollr', get_stylesheet_directory_uri() . '/js/vendor/skrollr/skrollr.min.js', false, 1, true);
		    wp_register_script('wow', get_stylesheet_directory_uri() . '/js/vendor/wow/wow.min.js', false, 1, true);
        }
				
				wp_register_script('what-input', get_stylesheet_directory_uri() . '/bower_components/what-input/what-input.js', false, 1, true);
				wp_register_script('foundation', get_stylesheet_directory_uri() . '/bower_components/foundation-sites/dist/foundation.min.js', false, 1, true);
				
				wp_enqueue_script('what-input');
				wp_enqueue_script('foundation');
	   wp_enqueue_script('gallery');
       wp_enqueue_script('skrollr');
       wp_enqueue_script('wow');			
       wp_enqueue_script('child-scripts', array('slick'));
      
        

	}

	//////////////////////////////////////////////////////////////////
	// We want to custom theme this administration panel
	//////////////////////////////////////////////////////////////////

	function custom_loginlogo() {
		echo '
		<style type="text/css">
			h1 a { background-image: url('.get_stylesheet_directory_uri().'/img/vargo.svg) !important; min-width: 300px !important; min-height: 100px !important; margin: 0 auto !important; position: relative !important; z-index: 10000 !important; background-size: auto !important; }
		</style>';
	}

	add_action('login_head', 'custom_loginlogo');

?>
