<aside class="widget menu-widget">
	
<?php
	$children = wp_list_pages('depth=1&title_li=&child_of='.$post->ID.'&echo=0');
	$current_page = wp_list_pages('title_li=&include='.$post->ID.'&echo=0');
	$grandparent=0;
		if($post->post_parent):
			$parent = wp_list_pages('title_li=&include='.$post->post_parent.'&echo=0');
			$siblings = wp_list_pages('depth=1&title_li=&child_of='.$post->post_parent.'&exclude='.$post->ID.'&echo=0');
			$parent_children = wp_list_pages('depth=1&title_li=&child_of='.$post->post_parent.'&echo=0');
			$parent_post = get_post($post->post_parent);
			$grandparent = $parent_post->post_parent;
			$parent_siblings = wp_list_pages('depth=1&title_li=&child_of='.$grandparent.'&echo=0&exclude='.$post->post_parent);
		endif;
		if(!$post->post_parent && $children):
?>

	<ul class="subpages">
		<h3 class="parent"><?php echo get_the_title( $post->post_parent ); ?></h3>
		<?php echo $children;?>
	</ul><!-- /.subpages -->

<?php elseif($post->post_parent && $children): ?>

	<ul class="subpages">
		<h3 class="parent"><?php echo get_the_title( $post->post_parent ); ?></h3>			
			<?php echo $children;?>			
			<?php echo $siblings;?>
	</ul><!-- /.subpages -->

<?php elseif($grandparent && $post->post_parent && !$children): ?>

	<ul class="subpages">		
		<h3 class="parent"><?php echo get_the_title( $post->post_parent ); ?></h3>			
			<?php echo $parent_children;?>			
			<?php echo $parent_siblings;?>		
	</ul><!-- /.subpages -->

<?php elseif(!$grandparent && $post->post_parent && !$children): ?>

	<ul class="subpages">
		<h3 class="parent"><?php echo get_the_title( $post->post_parent ); ?></h3>			
			<?php echo $parent_children;?>					
	</ul><!-- /.subpages -->
	
<?php else : ?>

	<h3 class="parent"><?php the_field('navigation_header', 'option'); ?></h3>
	<?php $sidebar_menu = get_field( 'sidebar_menu', 'option' );
  $defaults = array(
    'theme_location'  => 'sidebar_menu',
    'menu'            => $sidebar_menu,
    'container'       => 'div',
    'container_class' => '',
    'container_id'    => '',
    'menu_class'      => '',
    'menu_id'         => '',
    'echo'            => true,
    'fallback_cb'     => 'wp_page_menu',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth'           => 1,
    'walker' 		  => new GDS_Walker()
  );

  wp_nav_menu( $defaults ); ?>

<?php endif;?>	

</aside> <!-- /.menu-widget -->

<?php if( get_field('call_to_action', 'option') ): ?>
  <aside class="widget text-widget">
  	<?php the_field('call_to_action', 'option'); ?>
  </aside> <!-- /.text-widget -->
<?php endif; ?>

<?php if( get_field('customer_review', 'option') ): ?>
  <aside class="widget text-widget">
    <div class="sidebar-testimonial">
  	<?php the_field('customer_review', 'option'); ?>
    </div> <!-- /.sidebar-testimonial -->
  </aside> <!-- /.testimonial-widget -->
<?php endif; ?>

<?php
$post_object = get_field('choose_your_cta');
if( $post_object ):

// override $post
$post = $post_object;
setup_postdata( $post );
?>

<aside class="widget text-widget">
<div class="sidebar-testimonial call-to-action">
    <div class="cta-<?php the_ID(); ?>">
      <?php the_post_thumbnail(); ?>      
     	 <h4><?php the_title();?></h4>
    	<?php the_content(); ?>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
</div><!-- /.sidebar-testimonial -->
</aside><!-- /.widget .text-widget -->
<?php endif; ?>


