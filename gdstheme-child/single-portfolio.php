<?php get_header(); ?>

<?php get_template_part('inc/modules/content', 'title'); ?>
<div class="content-container">
        <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
        <div class="row breadcrumb-row">
          <div class="medium-12 columns">
            <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
          </div>
        </div>
        <?php } ?>
  			<div class="row">
  			<div class="large-12 columns">
	
			<!-- Gallery wrapper -->
			
			<?php
				$images = get_field('photo_gallery');
				if( $images ):
			?>
			
			<ul class="small-up-1 medium-up-4 gallery" data-clearing data-accessible>
		        <?php foreach( $images as $image ): ?>
		            <li class="grid-item column-block columns">
		            		
		                	<a href="<?php echo $image['sizes']['large']; ?>" class="gallery-thumb thumbnail">
			                	<span class="icon icon-expand"></span>
			                	<img src="<?php echo $image['sizes']['mini-gallery']; ?>" alt="<?php echo $image['alt']; ?>" />
			                </a>              						
		                    
		            </li>
		        <?php endforeach; ?>
		    </ul>
		    
			<?php endif; ?>

		</div><!-- /#content -->
	  
	</div> <!-- /.wrap -->
	
</div> <!-- /.container -->

<?php get_footer(); ?>