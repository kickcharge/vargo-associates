
<?php /* Template Name: Portfolio Page */ ?>
<?php get_header(); ?>

      <?php get_template_part('inc/modules/content', 'title'); ?>
      <div class="content-container">
        <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
        <div class="row breadcrumb-row">
          <div class="medium-12 columns">
            <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
          </div>
        </div>
        <?php } ?>
  			<div class="row">
  	      <div class="medium-12 columns">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			  		<?php the_content(); ?>
			<?php endwhile; ?>
			<?php else : ?>
			  	<h2>Sorry Nothing Found</h2>
			<?php endif; ?>		
			
			<div class="row">
		  			
	  			<div class="large-12 columns">
	  			<ul class="small-block-grid-2 medium-block-grid-4 large-block-grid-6 gallery" data-equalizer data-clearing data-accessible>
				<!-- Gallery wrapper -->
					<?php						
			
					$args = array( 
						'post_type' => 'portfolio', 
						'posts_per_page' => '-1', 
						'post_parent' => 0, 
						'orderby'=>'date',
						'order'=>'DESC', );						
					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>	
					
					
						
					        
					            <li class="grid-item medium-4 small-12 columns" data-equalizer-watch>
					            		
					                	<a href="<?php the_permalink(); ?>" class="">
						                	<span class="icon icon-expand"><?php the_title();?></span>
						                	<?php echo get_the_post_thumbnail($post_id, 'large'); ?>
						                </a>              						
					                    
					            </li>
					        
					    				    
					
					<?php endforeach; 
					wp_reset_postdata();?>
					</ul>
	
			</div><!-- /#content -->
		  
		</div> <!-- /.row -->
			
  	    </div>
          
          <?php get_template_part('inc/acf/page', 'builder'); ?>
        </div>
      </div>

<?php get_footer(); ?>
