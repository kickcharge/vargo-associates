<footer class="hide-for-print">
  <?php if(get_field('image_or_color', 'option') == 'color') : ?>
    <div id="parallax3" class="contact-form parallax section row" style="background-color: <?php the_field('background_color', 'option'); ?>">
      <div class="large-12 columns wow fadeIn" data-wow-offset="300">
        <?php the_field('footer_tier_1_content', 'option'); ?>
      </div> <!-- /.wrap -->
    </div> <!-- /#parallax3 .contact-form -->
  <?php elseif(get_field('image_or_color', 'option') == 'image') : ?>
  <?php
    $image = get_field('tier_1_parallax_image', 'option');

    // vars
    $url = $image['url'];
    $title = $image['title'];
    $alt = $image['alt'];
    $caption = $image['caption'];

    // image size
    $size = 'full';
    $thumb = $image['sizes'][ $size ];
    $width = $image['sizes'][ $size . '-width' ];
    $height = $image['sizes'][ $size . '-height' ];

    if( !empty($image) ):
  ?>
    <div
      id="parallax3"
      class="contact-form parallax"
      style="background-image: url(<?php echo $image['url']; ?>); background-repeat: no-repeat;"
      data-start="background-position: center 0%"
      data-end="background-position: center 100%"
    >	
	  <div class="row">
      <div class="large-12 columns wow fadeIn" data-wow-offset="300">
        <?php the_field('footer_tier_1_content', 'option'); ?>
      </div> <!-- /.wrap -->
	  </div>
    </div><!-- /#parallax3 .contact-form -->
  <?php endif;else : endif; ?>
  <div class="footer-tier-2">
    <div class="sitemap wow fadeIn row" data-wow-offset="300">
      <div class="large-12 columns">
        <?php if( get_field( 'footer_navigation', 'option' ) ) : ?>
          <?php
            $main_menu = get_field('footer_navigation', 'option');
            $defaults = array(
              'theme_location'  => 'primary_menu',
              'menu'            => $main_menu,
              'container'       => 'div',
              'container_class' => 'menu menu-centered',
              'container_id'    => '',
              'menu_class'      => 'menu vertical large-horizontal',
              'menu_id'         => 'main_menu',
              'echo'            => true,
              'fallback_cb'     => 'wp_page_menu',
              'before'          => '',
              'after'           => '',
              'link_before'     => '',
              'link_after'      => '',
              'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              'depth'           => 1,
              'walker' 		  => new GDS_Walker()
            );
            wp_nav_menu( $defaults );
          ?>

      <?php endif; ?>
      </div> <!-- /.wrap -->
    </div> <!-- /.sitemap -->   

    <div class="credits row">
      <div class="large-12 columns">
        <?php the_field('footer_tier_2_content', 'option'); ?>
      </div> <!-- /.wrap -->
    </div> <!-- /.credits -->
    
    <div class="social wow fadeIn row" data-wow-offset="200">
      <div class="large-12 columns">
        <?php if(get_field('social_media_navigation', 'option') ) : ?>
          <?php the_field('social_media_navigation', 'option'); ?>
      <?php endif; ?>
      </div> <!-- /.wrap -->
    </div> <!-- /.social -->
    
    <p style="text-align: center;"><img class="aligncenter size-full wp-image-33" src="http://projects.graphicd-signs.com/vargo/site/wp-content/uploads/2016/11/footer_logo.png" alt="footer_logo" width="55" height="56" /></p>

  </div><!-- /.footer-tier-2-->

</footer>

<div class="reveal large" id="galleryModal" data-animation-in="fade-in" data-animation-out="fade-out" data-reveal data-v-offset="160">
	<div class="rev"> 
		<button class="prev-button" type="button">
			<span><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
		</button>
	
		<button class="next-button" type="button">
			<span><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
		</button>
	
		<button class="close-button" data-close aria-label="Close modal" type="button">
			<span>&times;</span>
		</button>
		
		<img class="js-modal-preview modal-preview" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
</div>

</div>
</div> <!-- Close offcanvas wrappers -->
</div>
<?php wp_footer(); ?>
</body>
</html>
