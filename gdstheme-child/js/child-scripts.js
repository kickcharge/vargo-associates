// Child Theme Scripts

jQuery(document).ready(function($) {

	$(document).foundation();	
	
	// Assign unique ID's to each section on the home page from the builders
	$( 'section[data-field=field_55f734f9496af]',this ).attr( "id", function( arr ) {
		return "open_ended_" + (arr+1);
	});

	$( 'section[data-field=field_55f82d029aed3]',this ).attr( "id", function( arr ) {
		return "open_ended_parallax_" + (arr+1);
	});

	$( 'section[data-field=field_55f7340381455]',this ).attr( "id", function( arr ) {
		return "idea-gallery_" + (arr+1);
	});

	$( 'section[data-field=field_55f73497352c2]',this ).attr( "id", function( arr ) {
		return "idea-gallery_case_" + (arr+1);
	});

	$( 'section[data-field=field_55f732af81454]',this ).attr( "id", function( arr ) {
		return "feed_" + (arr+1);
	});

	$( 'section[data-field=field_55f82fac3a34e]',this ).attr( "id", function( arr ) {
		return "services_" + (arr+1);
	});

	$( 'section[data-field=field_563121aba5fff]',this ).attr( "id", function( arr ) {
		return "testimonial_slider_" + (arr+1);
	});
	
	$( 'section[data-field=field_563cf47caf1d2]',this ).attr( "id", function( arr ) {
		return "custom_content_field_" + (arr+1);
	});
	
	$( 'section[data-field=field_55f82ddd9aed8]',this ).attr( "id", function( arr ) {
		return "statistics_content_field_" + (arr+1);
	});
	
	$( 'section[data-field=field_57275a7e37054]',this ).attr( "id", function( arr ) {
		return "cpt_data_" + (arr+1);
	});

	var main_menu = new Foundation.DropdownMenu($('#main_menu'));
	var main_menu_offcanvas = new Foundation.Drilldown($('#main_menu_offcanvas'), {parentLink: true});

	if ( $('body').hasClass('single-coupons') ) {
		window.print();
	}
	
	
	// Skrollr
	skrollr.init({
		forceHeight: false,
		mobileCheck: function() {
			return false;
		}
	});
	

});

new WOW().init();