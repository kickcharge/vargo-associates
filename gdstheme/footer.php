<footer class="hide-for-print">
  <?php if(get_field('image_or_color', 'option') == 'color') : ?>
    <div id="parallax3" class="contact-form parallax section" style="background-color: <?php the_field('background_color', 'option'); ?>">
      <div class="wrap wow fadeIn" data-wow-offset="300">
        <?php the_field('footer_tier_1_content', 'option'); ?>
      </div> <!-- /.wrap -->
    </div> <!-- /#parallax3 .contact-form -->
  <?php elseif(get_field('image_or_color', 'option') == 'image') : ?>
  <?php
    $image = get_field('tier_1_parallax_image', 'option');

    // vars
    $url = $image['url'];
    $title = $image['title'];
    $alt = $image['alt'];
    $caption = $image['caption'];

    // image size
    $size = 'full';
    $thumb = $image['sizes'][ $size ];
    $width = $image['sizes'][ $size . '-width' ];
    $height = $image['sizes'][ $size . '-height' ];

    if( !empty($image) ):
  ?>
    <div
      id="parallax3"
      class="contact-form parallax"
      style="background-image: url(<?php echo $image['url']; ?>); background-repeat: no-repeat;"
      data-start="background-position: center 0%"
      data-end="background-position: center 100%"
    >
      <div class="wrap wow fadeIn" data-wow-offset="300">
        <?php the_field('footer_tier_1_content', 'option'); ?>
      </div> <!-- /.wrap -->
    </div><!-- /#parallax3 .contact-form -->
  <?php endif;else : endif; ?>
  <div class="footer-tier-2">
    <div class="sitemap wow fadeIn" data-wow-offset="300">
      <div class="wrap">
        <?php if( get_field( 'footer_navigation', 'option' ) ) : ?>
          <?php
            $main_menu = get_field('footer_navigation', 'option');
            $defaults = array(
              'theme_location'  => 'primary_menu',
              'menu'            => $main_menu,
              'container'       => 'div',
              'container_class' => 'menu menu-centered',
              'container_id'    => '',
              'menu_class'      => 'menu vertical large-horizontal',
              'menu_id'         => 'main_menu',
              'echo'            => true,
              'fallback_cb'     => 'wp_page_menu',
              'before'          => '',
              'after'           => '',
              'link_before'     => '',
              'link_after'      => '',
              'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              'depth'           => 1,
              'walker' 		  => new GDS_Walker()
            );
            wp_nav_menu( $defaults );
          ?>

      <?php endif; ?>
      </div> <!-- /.wrap -->
    </div> <!-- /.sitemap -->

    <div class="social wow fadeIn" data-wow-offset="200">
      <div class="wrap">
        <?php if(get_field('social_media_navigation', 'option') ) : ?>
          <?php the_field('social_media_navigation', 'option'); ?>
      <?php endif; ?>
      </div> <!-- /.wrap -->
    </div> <!-- /.social -->

    <div class="credits">
      <div class="wrap">
        <?php the_field('footer_tier_2_content', 'option'); ?>
      </div> <!-- /.wrap -->
    </div> <!-- /.credits -->

  </div><!-- /.footer-tier-2-->

</footer>

</div>
</div> <!-- Close offcanvas wrappers -->
</div>
<?php wp_footer(); ?>
</body>
</html>
