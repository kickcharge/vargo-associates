      <?php get_header(); ?>

      <?php get_template_part('inc/modules/content', 'title'); ?>

      <div class="content-container">
        <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
        <div class="row breadcrumb-row">
          <div class="medium-12 columns">
            <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
          </div>
        </div>
        <?php } ?>
        <div class="row">
          <div class="medium-8 columns">
            <?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

              <?php
                if ( has_post_thumbnail() ) {
                  echo '<div class="featured-post-image">';
                  the_post_thumbnail('featured-full');
                  echo '</div>';
                }
              ?>

              <h2 class="post-title"><?php the_title(); ?></h2>

              <div class="post-info">
                <div class="author"><?php the_author(); ?></div> <!-- /.author -->
                <div class="date"><?php the_time('j F') ?></div> <!-- /.date -->                 
              </div> <!-- /.post-info -->

              <?php the_content()?>

            <?php } ?>

            <?php } else { ?>

              <h1>No content found.</h1>

            <?php } ?>
            
          </div>
          <?php get_sidebar('right'); ?>
          <?php get_template_part('inc/acf/page', 'builder'); ?>
        </div>
      </div>
<?php get_footer(); ?>
