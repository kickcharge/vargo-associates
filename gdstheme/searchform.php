<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
	<div class="input-group">
		<input type="text" value="<?php the_search_query(); ?>" name="s" id="s" class="input-group-field" placeholder="Search" />
		<div class="input-group-button">
			<button class="button" type="submit"><i class="fa fa-search"></i></button>
		</div>
	</div>
</form>
