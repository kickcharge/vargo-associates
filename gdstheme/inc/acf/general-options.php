<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_55522d4e16e53',
	'title' => 'General Options',
	'fields' => array (
		array (
			'key' => 'field_555250fb590d2',
			'label' => 'Company Logo',
			'name' => 'company_logo',
			'type' => 'image',
			'instructions' => 'Please upload your logo file here.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_55c8f9731cb77',
			'label' => 'Breadcrumbs Positioning',
			'name' => 'breadcrumbs_positioning',
			'type' => 'radio',
			'instructions' => 'Where would you like your breadcrumbs to display on your page?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'header' => 'Header',
				'content' => 'Content Area',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_55f9ac877a60f',
			'label' => 'Image Hover Effect',
			'name' => 'image_hover_effect',
			'type' => 'radio',
			'instructions' => 'Use this selector to choose the type of image hover style you would like.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'shrink' => 'Shrink On Hover',
				'enlarge' => 'Enlarge On Hover',
				'blur' => 'Blur On Hover',
				'decolorize' => 'Decolorize On Hover',
				'darken' => 'Darken On Hover',
				'opacity' => 'Opacity Adjust On Hover',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
		),
		array (
			'key' => 'field_562a64aa1488b',
			'label' => 'Custom Section',
			'name' => 'custom_section',
			'type' => 'checkbox',
			'instructions' => 'Please use this field to enable custom content sections for your site.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'case_studies' => 'Case Studies',
				'portfolio' => 'Portfolio',
				'testimonials' => 'Testimonials',
				'products' => 'Products',
				'coupons' => 'Coupons',
				'ctas' => 'Custom Calls To Actions',
				'team' => 'Team Members',
			),
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-global-styles',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;	
	
?>