<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_569d50467d54b',
	'title' => 'Product Catalog',
	'fields' => array (
		array (
			'key' => 'field_569d5053d167b',
			'label' => 'Optional Product Catalog Components',
			'name' => 'optional_product_catalog_components',
			'type' => 'flexible_content',
			'instructions' => 'Use this section to add some additional features to your products page.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => 'Add Row',
			'min' => '',
			'max' => '',
			'layouts' => array (
				array (
					'key' => '569d506d83dfb',
					'name' => 'optional_product_catalog_components',
					'label' => 'Optional Product Catalog Components',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_569d521515f28',
							'label' => 'Product Gallery',
							'name' => 'product_gallery',
							'type' => 'gallery',
							'instructions' => 'Use this field to upload additional photos of your product.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => '',
							'preview_size' => 'thumbnail',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => '',
							'max_height' => '',
							'max_size' => '',
							'mime_types' => '',
							'library' => 'all',
							'insert' => 'append',
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '569d536215f29',
					'name' => 'additional_content',
					'label' => 'Additional Content',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_569d536f15f2a',
							'label' => 'Additional Content',
							'name' => 'additional_content',
							'type' => 'wysiwyg',
							'instructions' => 'Need another section to populated segmented content? Use this field.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 1,
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '569d5412d53c9',
					'name' => 'product_downloads',
					'label' => 'Product Downloads',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_569d543fd53cb',
							'label' => 'File Uploads',
							'name' => 'file_uploads',
							'type' => 'repeater',
							'instructions' => 'Do you need to upload product downloads, such as manuals, images, warranty information? Use this section to upload as many files as needed in a list format.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'collapsed' => 'field_569d5477d53cc',
							'min' => '',
							'max' => '',
							'layout' => 'table',
							'button_label' => 'Add Product Files',
							'sub_fields' => array (
								array (
									'key' => 'field_569d5477d53cc',
									'label' => 'Product File',
									'name' => 'product_file',
									'type' => 'file',
									'instructions' => 'Upload product file here.',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'return_format' => 'array',
									'library' => 'all',
									'min_size' => '',
									'max_size' => '',
									'mime_types' => '',
								),
							),
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '569d56d7b796f',
					'name' => 'related_products',
					'label' => 'Related Products',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_569d56e2b7970',
							'label' => 'Related Products',
							'name' => 'related_products',
							'type' => 'post_object',
							'instructions' => 'Would you like to show related products? Use this field to choose up to 4 similar products.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'post_type' => array (
								0 => 'products',
							),
							'taxonomy' => array (
							),
							'allow_null' => 0,
							'multiple' => 1,
							'return_format' => 'object',
							'ui' => 1,
						),
					),
					'min' => '',
					'max' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'products',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_5768383602e35',
	'title' => 'Product Description',
	'fields' => array (
		array (
			'key' => 'field_5768383dfadcc',
			'label' => 'Description',
			'name' => 'description',
			'type' => 'text',
			'instructions' => 'Describe the product page here.',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_57698ea4b995f',
			'label' => 'Product Image',
			'name' => 'product_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'products',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

 ?>
