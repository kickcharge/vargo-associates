<?php

/* ------------------------------------------------------------- */
/*	Enque Scripts / Styles
/* ------------------------------------------------------------- */

function myshortcodes_enqueue_scripts() {
	wp_register_script('scripts', get_template_directory_uri() . '/inc/shortcodes/js/scripts.js', true);
	wp_enqueue_style('shortcodes', get_template_directory_uri() . "/inc/shortcodes/shortcodes.css");
	wp_enqueue_script('scripts');
	wp_enqueue_style('shortcodes.css');
}
add_action('wp_enqueue_scripts', 'myshortcodes_enqueue_scripts');

/* ------------------------------------------------------------- */
/*	YouTube
/* ------------------------------------------------------------- */

add_shortcode('youtube', 'shortcode_youtube');
	function shortcode_youtube($atts) {
		$atts = shortcode_atts(
			array(
				'id' => '',
				'width' => 600,
				'height' => 360
			), $atts);

			return '<div class="video-shortcode"><iframe title="YouTube video player" width="' . $atts['width'] . '" height="' . $atts['height'] . '" src="https://www.youtube.com/embed/' . $atts['id'] . '" frameborder="0" allowfullscreen></iframe></div>';
	}

/* ------------------------------------------------------------- */
/*	Vimeo
/* ------------------------------------------------------------- */

add_shortcode('vimeo', 'shortcode_vimeo');
	function shortcode_vimeo($atts) {
		$atts = shortcode_atts(
			array(
				'id' => '',
				'width' => 600,
				'height' => 360
			), $atts);

			return '<div class="video-shortcode"><iframe src="https://player.vimeo.com/video/' . $atts['id'] . '" width="' . $atts['width'] . '" height="' . $atts['height'] . '" frameborder="0"></iframe></div>';
	}
	
/* ------------------------------------------------------------- */
/*	SoundCloud
/* ------------------------------------------------------------- */

add_shortcode('soundcloud', 'shortcode_soundcloud');
	function shortcode_soundcloud($atts) {
		$atts = shortcode_atts(
			array(
				'url' => '',
				'width' => '100%',
				'height' => 81,
				'comments' => 'true',
				'auto_play' => 'true',
				'color' => 'ff7700',
			), $atts);

			return '<object height="' . $atts['height'] . '" width="' . $atts['width'] . '"><param name="movie" value="http://player.soundcloud.com/player.swf?url=' . urlencode($atts['url']) . '&amp;show_comments=' . $atts['comments'] . '&amp;auto_play=' . $atts['auto_play'] . '&amp;color=' . $atts['color'] . '"></param><param name="allowscriptaccess" value="always"></param><embed allowscriptaccess="always" height="' . $atts['height'] . '" src="https://player.soundcloud.com/player.swf?url=' . urlencode($atts['url']) . '&amp;show_comments=' . $atts['comments'] . '&amp;auto_play=' . $atts['auto_play'] . '&amp;color=' . $atts['color'] . '" type="application/x-shockwave-flash" width="' . $atts['width'] . '"></embed></object>';
	}

/* ------------------------------------------------------------- */
/*	Button
/* ------------------------------------------------------------- */

add_shortcode('gds_button', 'shortcode_gds_button');
	function shortcode_gds_button($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
		$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
		$atts = shortcode_atts(
			array(
				'color' => 'black',
				'link' => '#',
				'target' => '',
			), $atts);

			return '<a class="button expanded" href="' . $atts['link'] . '" target="' . $atts['target'] . '">' .do_shortcode($content). '</a>';
	}
	
/* ------------------------------------------------------------- */
/*	Dropcap
/* ------------------------------------------------------------- */

add_shortcode('dropcap', 'shortcode_dropcap');
	function shortcode_dropcap( $atts, $content = null ) {

		return '<span class="dropcap">' .do_shortcode($content). '</span>';

}

/* ------------------------------------------------------------- */
/*	Highlight
/* ------------------------------------------------------------- */

add_shortcode('highlight', 'shortcode_highlight');
	function shortcode_highlight($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
		$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
		$atts = shortcode_atts(
			array(
				'color' => 'yellow',
			), $atts);

			if($atts['color'] == 'black') {
				return '<span class="highlight2">' .do_shortcode($content). '</span>';
			} else {
				return '<span class="highlight1">' .do_shortcode($content). '</span>';
			}

	}

/* ------------------------------------------------------------- */
/*	Checklist
/* ------------------------------------------------------------- */

add_shortcode('checklist', 'shortcode_checklist');
	function shortcode_checklist( $atts, $content = null ) {
	$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	$content = str_replace('<ul>', '<ul class="checklist">', do_shortcode($content));
	$content = str_replace('<li>', '<li>', do_shortcode($content));

	return $content;

}

/* ------------------------------------------------------------- */
/*	Bad List
/* ------------------------------------------------------------- */

add_shortcode('badlist', 'shortcode_badlist');
	function shortcode_badlist( $atts, $content = null ) {
	$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	$content = str_replace('<ul>', '<ul class="badlist">', do_shortcode($content));
	$content = str_replace('<li>', '<li>', do_shortcode($content));

	return $content;

}

/* ------------------------------------------------------------- */
/*	Tabs
/* ------------------------------------------------------------- */

add_shortcode('tabs', 'shortcode_tabs');
	function shortcode_tabs( $atts, $content = null ) {
	extract(shortcode_atts(array(
    ), $atts));

	$out .= '<div class="tabs-wrapper">';

	$out .= '<ul class="tabs">';
	foreach ($atts as $key => $tab) {
		$out .= '<li><a href="#' . $key . '">' . $tab . '</a></li>';
	}
	$out .= '</ul>';

	$out .= '<div class="tabs_container">';

	$out .= do_shortcode($content) .'</div></div>';

	return $out;
}

add_shortcode('tab', 'shortcode_tab');
	function shortcode_tab( $atts, $content = null ) {
	$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	extract(shortcode_atts(array(
    ), $atts));

	$out .= '<div id="tab' . $atts['id'] . '" class="tab_content">' . do_shortcode($content) .'</div>';

	return $out;
}

/* ------------------------------------------------------------- */
/*	Toggle
/* ------------------------------------------------------------- */

add_shortcode( 'toggle', 'shortcode_toggle' );
	function shortcode_toggle( $atts, $content = null ) {
	$content = preg_replace( '#^<\/p>|<p>$#', '', $content );
	$content = preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
	extract(shortcode_atts(array(
        'title'      => ''
    ), $atts));

	$out .= '<ul class="accordion" data-accordion data-allow-all-closed="true">';
	$out .= '  <li class="accordion-item" data-accordion-item>';
	$out .= '    <a href="#" class="accordion-title">'.$title.'</a>';
	$out .= '    <div class="accordion-content" data-tab-content>';
	$out .= '      ' . do_shortcode($content);
	$out .= '    </div>';
	$out .= '  </li>';
	$out .= '</ul>';

   return $out;
}

/* ------------------------------------------------------------- */
/*	Column - 1/2
/* ------------------------------------------------------------- */

add_shortcode( 'one_half', 'shortcode_one_half' );
	function shortcode_one_half( $atts, $content = null ) {
		$content = preg_replace( '#^<\/p>|<p>$#', '', $content );
		$content = preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
		$atts = shortcode_atts(
			array(
				'last' => 'no',
			), $atts);

			if($atts['last'] == 'yes') {
				return '<div class="large-6 columns">' . do_shortcode( $content ) . '</div><div class="clearfix"></div>';
			} else {
				return '<div class="large-6 columns">' . do_shortcode( $content ) . '</div>';
			}

	}

/* ------------------------------------------------------------- */
/*	Column - 1/3
/* ------------------------------------------------------------- */

add_shortcode( 'one_third', 'shortcode_one_third' );
	function shortcode_one_third( $atts, $content = null ) {
		$content = preg_replace( '#^<\/p>|<p>$#', '', $content );
		$content = preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
		$atts = shortcode_atts(
			array(
				'last' => 'no',
			), $atts);

			if($atts['last'] == 'yes') {
				return
					'<div class="large-4 columns">' . do_shortcode( $content ) . '</div><div class="clearfix"></div>';
			} else {
				return
					'<div class="large-4 columns">' . do_shortcode( $content ) . '</div>';
			}

	}

/* ------------------------------------------------------------- */
/*	Column - 2/3
/* ------------------------------------------------------------- */

add_shortcode('two_third', 'shortcode_two_third');
	function shortcode_two_third($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
		$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
		$atts = shortcode_atts(
			array(
				'last' => 'no',
			), $atts);

			if($atts['last'] == 'yes') {
				return '<aside class="large-8 columns">'.do_shortcode($content).'</aside><div class="clearfix"></div>';
			} else {
				return '<aside class="large-8 columns">'.do_shortcode($content).'</aside>';
			}

	}

/* ------------------------------------------------------------- */
/*	Column - 1/4
/* ------------------------------------------------------------- */

add_shortcode( 'one_fourth', 'shortcode_one_fourth' );
	function shortcode_one_fourth( $atts, $content = null ) {
		$content = preg_replace( '#^<\/p>|<p>$#', '', $content );
		$content = preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
		$atts = shortcode_atts(
			array(
				'last' => 'no',
			), $atts);

			if($atts['last'] == 'yes') {
				return '<div class="large-3 columns">' . do_shortcode( $content ) . '</div><div class="clearfix"></div>';
			} else {
				return '<div class="large-3 columns">' . do_shortcode( $content ) . '</div>';
			}

	}

/* ------------------------------------------------------------- */
/*	Column - 3/4
/* ------------------------------------------------------------- */

add_shortcode('three_fourth', 'shortcode_three_fourth');
	function shortcode_three_fourth($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
		$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
		$atts = shortcode_atts(
			array(
				'last' => 'no',
			), $atts);

			if($atts['last'] == 'yes') {
				return '<aside class="three_fourth last">'.do_shortcode($content).'</aside><div class="clearfix"></div>';
			} else {
				return '<aside class="three_fourth">'.do_shortcode($content).'</aside>';
			}

	}

/* ------------------------------------------------------------- */
/*	Line Seperator
/* ------------------------------------------------------------- */

function gw_printcode($atts, $content = null){
    return do_shortcode(str_replace("[", "&#91;", $content));
}
add_shortcode('printcode', 'gw_printcode');

/* divider shortcode */
function gw_line( $atts, $content = null ) {
   return '<div class="line"></div>';
}
add_shortcode('line', 'gw_line');

/* thicker divider shortcode */
function gw_thick_line( $atts, $content = null ) {
   return '<div class="thick-line"></div>';
}
add_shortcode('thick_line', 'gw_thick_line');

/* ------------------------------------------------------------- */
/*	Message / Warning Boxes
/* ------------------------------------------------------------- */

/* message box shortcode */
function gw_message_box( $atts, $content = null ) {
	$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	extract(shortcode_atts(array(
        'type'      => '',
    ), $atts));
    return '<div class="box-'.$type.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('message_box', 'gw_message_box');

/* blockquote shortcode */
function gw_blockquote( $atts, $content = null ) {
   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
   $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
   return '<blockquote>' . do_shortcode($content) . '</blockquote>';
}
add_shortcode('blockquote', 'gw_blockquote');

/* pullquote left shortcode */
function gw_pullquote_left( $atts, $content = null ) {
   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
   $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
   return '<span class="pullquote-left">' . do_shortcode($content) . '</span>';
}
add_shortcode('pullquote_left', 'gw_pullquote_left');

/* pullquote right shortcode */
function gw_pullquote_right( $atts, $content = null ) {
   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
   $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
   return '<span class="pullquote-right">' . do_shortcode($content) . '</span>';
}
add_shortcode('pullquote_right', 'gw_pullquote_right');

/* ------------------------------------------------------------- */
/*	Video
/* ------------------------------------------------------------- */

add_shortcode('video', 'shortcode_video');
	function shortcode_video( $atts, $content = null ) {
	$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	extract(shortcode_atts(array(
        'width'      => '',
        'height'      => '',
        'poster'      => '',
        'class'      => '',
        'src'		=> '',
    ), $atts));

	$out .= '<video class="'.$class.'" width="'.$width.'" height="'.$height.'" poster="'.$poster.'">';
	$out .= '<source src="'.$src.'"/>';
	$out .= do_shortcode($content);
	$out .= '</video>';

   return $out;
}

/* ------------------------------------------------------------- */
/*	Add Buttons to TinyMCE
/* ------------------------------------------------------------- */

add_action('init', 'add_button');

function add_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_plugin');
     add_filter('mce_buttons_3', 'register_button');
   }
}

function register_button($buttons) {
   array_push($buttons, "youtube", "vimeo", "soundcloud", "gds_button", "dropcap", "highlight", "checklist", "badlist", "tabs", "toggle", "one_half", "one_third", "two_third", "one_fourth", "three_fourth", "info", "confirmation", "warning", "error", "video");
   return $buttons;
}

function add_plugin($plugin_array) {
   $plugin_array['youtube'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['vimeo'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['soundcloud'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['gds_button'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['dropcap'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['highlight'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['checklist'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['badlist'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['tabs'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['toggle'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['one_half'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['one_third'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['two_third'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['one_fourth'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['three_fourth'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['info'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['confirmation'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['warning'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['error'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   $plugin_array['video'] = get_template_directory_uri().'/inc/shortcodes/tinymce/customcodes.js';
   return $plugin_array;
}

?>