<?php

if ( ! function_exists('team') ) {

// Register Custom Post Type
function team() {

	$labels = array(
		'name'                => _x( 'Team Members', 'Post Type General Name', 'gdstheme' ),
		'singular_name'       => _x( 'Team', 'Post Type Singular Name', 'gdstheme' ),
		'menu_name'           => __( 'Team Members', 'gdstheme' ),
		'name_admin_bar'      => __( 'Team Members', 'gdstheme' ),
		'parent_item_colon'   => __( 'Parent Item:', 'gdstheme' ),
		'all_items'           => __( 'All Team Members', 'gdstheme' ),
		'add_new_item'        => __( 'Add New Team Member', 'gdstheme' ),
		'add_new'             => __( 'Add New', 'gdstheme' ),
		'new_item'            => __( 'New Team Member', 'gdstheme' ),
		'edit_item'           => __( 'Edit Team Member', 'gdstheme' ),
		'update_item'         => __( 'Update Team Member', 'gdstheme' ),
		'view_item'           => __( 'View Team Member', 'gdstheme' ),
		'search_items'        => __( 'Search Team Members', 'gdstheme' ),
		'not_found'           => __( 'Not found', 'gdstheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'gdstheme' ),
	);
	$rewrite = array(
		'slug'                => 'team',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'Team', 'gdstheme' ),
		'description'         => __( 'This section is dedicated to creating team members within your website.', 'gdstheme' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'          => array(''),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-id',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'team', 0 );

}

?>