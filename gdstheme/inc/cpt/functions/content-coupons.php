		<?php
			$posts = new WP_Query(array(
				'post_type' => 'coupons',
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'ASC'
				)
			);
			while ( $posts->have_posts() ) : $posts->the_post();
		?>

		  <?php if (has_post_thumbnail( $post->ID ) ): ?>

			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

	          <div id="post-<?php the_ID(); ?>" <?php post_class('coupon'); ?> style="background-image: url('<?php echo $image[0]; ?>'); min-height: <?php echo $image[2]; ?>px; width: <?php echo $image[1]; ?>px;">

	          <?php endif; ?>

	          	<div class="coupon_content">

		           <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Coupon">

		          <h4><?php the_field('coupon_heading'); ?></h4>

		          <h5><?php the_field('coupon_subheading'); ?></h5>

		          <p class="terms"><?php the_field('coupon_terms'); ?></p>

		          <a href="#" class="button print-to-clip" onclick="window.print(); return false;">Click to Print</a>

	          	</div><!-- /.coupon_content -->

	          </div><!-- /.coupon -->

		<?php endwhile; wp_reset_postdata(); ?>