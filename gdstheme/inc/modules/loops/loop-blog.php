<div class="large-12 columns">
  <div class="feed">
    <div class="row small-up-1 medium-up-2 large-up-3" data-equalizer data-equalize-by-row="true">
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="column">
          <div class="callout" data-equalizer-watch>
            <div class="row">
              <div class="large-12 columns">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(300, 220) ); ?></a>
              </div>
            </div>
            <div class="description">
              <div class="row">
                <div class="large-12 columns">
                  <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                </div>
              </div>
              <div class="row date-row">
                <div class="large-12 columns">
                  <small class="text-center"><?php the_time('F j, Y'); ?></small>
                </div>
              </div>
              <div class="row">
                <div class="large-12 columns">
                  <p><?php echo excerpt(20); ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
</div>
