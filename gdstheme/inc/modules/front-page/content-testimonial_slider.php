  <?php
    $field = get_sub_field_object( 'testimonials' );
  ?>

<section class="testimonial_container" data-field="<?php echo $field['key']; ?>">

<?php the_sub_field('testimonials_content'); ?>

<?php

/*
*  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
*  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
*  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
*/

$post_objects = get_sub_field('testimonials');



if( $post_objects ): ?>
  <div class="testimonial_slider">
    <div class="orbit" role="region" aria-label="Hero Slider" data-orbit>
      <ul data-equalizer class="orbit-container" >
        <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
          <?php setup_postdata($post); ?>
          <li class="orbit-slide" >
            <?php the_content(); ?>
          </li>
        <?php endforeach; ?>
      </ul>
      <nav class="orbit-bullets">
        <?php for ($i = 0; $i < count($post_objects); $i++) { ?>
          <button data-slide="<?php echo $i; ?>" <?php if ($i == 0) { ?>class = "is-active"<?php }; ?>></button>
        <?php } ?>
      </nav>
    </div>
  </div><!-- /.testimonial_slider -->
  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

<a href="<?php echo get_post_type_archive_link( 'testimonials' ); ?>" class="button">Read More</a>

</section><!-- /.feed -->
