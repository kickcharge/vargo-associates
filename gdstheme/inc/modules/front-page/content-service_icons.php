<?php
  $field = get_sub_field_object( 'service' );
  $cols = get_sub_field('columns');
?>

<section class="service-icons" data-field="<?php echo $field['key']; ?>">

  <?php if (get_sub_field('section_title_services')) { ?>
    <div class="row title-row">
      <div class="large-12 columns">
        <?php the_sub_field('section_title_services'); ?>
      </div>
    </div>
  <?php } ?>

  <div class="row small-up-1 medium-up-<?php echo $cols; ?> wow fadeIn" data-equalizer data-wow-offset="200">
    <?php while(have_rows('service')) { the_row(); ?>
      <div class="column">
        <a href="<?php the_permalink(get_sub_field('service_link')); ?>">
          <div class="row">
            <div class="large-12 columns" data-equalizer-watch>
              <div class="image-container">
                <?php
                  $image = get_sub_field('service_icon');
                  if( !empty($image) ): ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" />
                <?php endif; ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <h4 class="text-center"><?php the_sub_field('service_title'); ?></h4>
            </div>
          </div>
        </a>
        <div class="row">
          <div class="large-12 columns text-center">
            <?php the_sub_field('service_description'); ?>
          </div>
        </div>
      </div>
    <?php }; ?>
  </div>

  <?php if (get_sub_field('add_button') == 'Yes') : ?>
      <div class="row">
        <div class="large-12 columns text-center">
          <a class="button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
        </div>
      </div>
    <?php endif; ?>
</section> <!-- /.services -->
