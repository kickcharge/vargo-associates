        <?php
          $image = get_sub_field('background_image');
          $watermark = get_sub_field('watermark');
          $field = get_sub_field_object( 'open_ended_content' );
        ?>
      <section
	    id="section-<?php echo $image['id']; ?>"
	    class="feed parallax section"
		data-start="background-position: center 0%"
	    data-end="background-position: center 100%"
	    data-field="<?php echo $field['key']; ?>"
        <?php if( !empty($image) ): ?>
	        style="background-image: url(<?php echo $image['url']; ?>); background-repeat: no-repeat;"
		<?php endif; ?>
	  >
	  <div class="bg_overlay">
		  <div class="watermark" style="background-image: url(<?php echo $watermark['url']; ?>);">
		  </div><!-- /.watermark -->
	  </div><!-- /.bg_overlay -->
        <div class="wrap wow fadeIn">
			<?php the_sub_field('open_ended_content'); ?>
        </div> <!-- /.wrap -->
      </section> <!-- /.feed -->
