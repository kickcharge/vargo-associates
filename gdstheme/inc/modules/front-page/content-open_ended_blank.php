<?php
	$field = get_sub_field_object( 'open_ended_content' );
?>

<section class="open_ended_content">
<?php the_sub_field('open_ended_content'); ?>
</section>
