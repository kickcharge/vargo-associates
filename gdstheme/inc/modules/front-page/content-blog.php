<?php if(get_sub_field('blog_type') == 'latest') { ?>
  <?php $field = get_sub_field_object( 'blog_posts' ); ?>
      <section class="feed section clearfix" data-field="<?php echo $field['key']; ?>">
        <div class="row">
        <div class="large-12 columns">
          <div class="row title-row">
            <div class="large-12 columns">
              <?php the_sub_field('section_title_blog'); ?>
            </div>
          </div>
          <div class="row small-up-1 medium-up-2 large-up-3 wow fadeIn" data-equalizer data-equalize-by-row="true" data-wow-offset="300">  
            <?php
              $posts = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => '3',
                'orderby' => 'date',
                'order' => 'ASC'
                )
              );
              while ( $posts->have_posts() ) : $posts->the_post();
            ?>
              <div class="column">
                <div class="callout" data-equalizer-watch>
                  <div class="row">
                    <div class="large-12 columns">
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(300, 220) ); ?></a>
                    </div>
                  </div>
                  <div class="description">
                    <div class="row">
                      <div class="large-12 columns">
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                      </div>
                    </div>
                    <div class="row date-row">
                      <div class="large-12 columns">
                        <small class="text-center"><?php the_time('F j, Y'); ?></small>
                      </div>
                    </div>
                    <div class="row">
                      <div class="large-12 columns">
                        <p><?php echo excerpt(20); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endwhile; wp_reset_postdata(); ?>
          </div>
          <div class="text-center">
            <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button"><?php the_sub_field('button_content_blog');?></a>
          </div>
        </div>
      </div>
    </section>

<?php } elseif(get_sub_field('blog_type') == 'manual') { ?>
  <?php $field = get_sub_field_object( 'blog_posts' ); ?>
      <section class="feed section clearfix" data-field="<?php echo $field['key']; ?>">
        <div class="row">
        <div class="large-12 columns">
          <div class="row">
            <div class="large-12 columns">
              <h1 class="text-center"><?php the_sub_field('section_title_blog'); ?></h3>
            </div>
          </div>
          <div class="row small-up-1 medium-up-2 large-up-3 wow fadeIn" data-equalizer data-equalize-by-row="true" data-wow-offset="300">
            <?php
              $post_objects = get_sub_field('blog_posts');
              if( $post_objects ):
              foreach( $post_objects as $post): // variable must be called $post (IMPORTANT)
            ?>
              <div class="column">
                <div class="callout" data-equalizer-watch>
                  <div class="row">
                    <div class="large-12 columns">
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(300, 220) ); ?></a>
                    </div>
                  </div>
                  <div class="description">
                    <div class="row">
                      <div class="large-12 columns">
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                      </div>
                    </div>
                    <div class="row date-row">
                      <div class="large-12 columns">
                        <small class="text-center"><?php the_time('F j, Y'); ?></small>
                      </div>
                    </div>
                    <div class="row">
                      <div class="large-12 columns">
                        <p><?php echo excerpt(20); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach; wp_reset_postdata(); endif; // IMPORTANT - reset the $post object so the rest of the page works correctly  ?>
          </div>
          <div class="row">
            <div class="medium-2 columns medium-centered">
              <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="alert button expanded"><?php the_sub_field('button_content_blog');?></a>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php } ?>
