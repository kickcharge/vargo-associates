        <?php
          $image = get_sub_field('section_background');
          $field = get_sub_field_object( 'custom_content' );
        ?>

      <section
      id=""
      class="feed parallax section cpt-data"
    data-start="background-position: center 0%"
      data-end="background-position: center 100%"
      data-field="<?php echo $field['key']; ?>"
        <?php if( !empty($image) ): ?>
          style="background-image: url(<?php echo $image['url']; ?>); background-repeat: no-repeat;"
    <?php endif; ?>
    >
        <div class="wrap wow fadeIn">

            <h2><?php the_sub_field('section_title'); ?></h2>

        <?php
            $post_objects = get_sub_field('custom_content');
            if( $post_objects ):
        ?>

        <div class="gallery-items">

      <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
          <?php setup_postdata($post); ?>

                <div class="gallery-item">
                  <a href="<?php the_permalink(); ?>">
                      <div class="gallery-item-img">
                <?php the_post_thumbnail('mini-gallery'); ?>
              </div> <!-- /.gallery-item-img -->
                      <div class="gallery-item-info">
                        <div class="gallery-item-info-wrap">
                          <div class="caption"><?php the_title(); ?></div> <!-- /.title -->
                          <div class="hoverlink fa fa-link">&nbsp;</div> <!-- /.icon-expand -->
                        </div> <!-- /.gallery-item-info-wrap -->
                      </div> <!-- /.gallery-item-info -->
                  </a>
                </div> <!-- /.gallery-item -->

                <?php endforeach; ?>

              </div> <!-- /.gallery-items -->

        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; // end of $post_objects ?>

        </div> <!-- /.wrap -->
      </section><!-- /.feed -->
