<section class="hero">
<?php if (get_sub_field('hero_slider') == 'Static') { ?>
  <div id="parallax1" class="hero-slide jumbotron parallax" data-start="background-position: center 0%" data-end="background-position: center 100%" style="background-image: url(<?php echo get_sub_field('hero_image')['url']; ?>); background-repeat: no-repeat;"> 
    <div class="hero-content text-center">
      <div class="hero-content-center">
        <?php the_sub_field('hero_content'); ?>
        <a class="button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
      </div>
    </div>
  </div>
<?php } else { ?>
  <div class="orbit" role="region" aria-label="Hero Slider" data-orbit data-options="animInFromLeft:scale-in-down; animInFromRight:scale-in-down; animOutToLeft:scale-out-down; animOutToRight:scale-out-down;">
    <ul data-equalizer class="orbit-container" >
      <?php if ( have_rows('slides') ) { ?>
        <?php while ( have_rows('slides') ) { the_row(); ?>
        <li class="orbit-slide" >
          <div class="hero-slide" style="background-image: url(<?php echo get_sub_field('slide_image')['url']; ?>);">
            <div data-equalizer-watch class="hero-content text-center">
              <div class="hero-content-center">
                <?php the_sub_field('slide_text'); ?>
                <a class="button" href="<?php the_sub_field('slide_button_link'); ?>"><?php the_sub_field('slide_button_text'); ?></a>
              </div>
            </div>
          </div>
        </li>
        <?php } ?>
      <?php } ?>
    </ul>
    <nav class="orbit-bullets">
      <?php for ($i = 0; $i < count(get_sub_field('slides')); $i++) { ?>
        <button data-slide="<?php echo $i; ?>" <?php if ($i == 0) { ?>class = "is-active"<?php }; ?>></button>
      <?php } ?>
    </nav>
  </div>
<?php }; ?>
</section>
