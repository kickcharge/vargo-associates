<aside class="widget menu-widget">

<?php $parentid = $post->post_parent ? @ array_pop( get_post_ancestors( $post ) ) : $post->ID; ?>

<ul class="subpages">
  <a class="parent" href="<?php echo get_permalink( $parentid ) ?>"><?php echo get_the_title( $parentid ) ?></a>
  <?php
	$children = new WP_Query(array(
    'post_parent' => $parentid,
    'post_type' => 'products',
		'order' => 'ASC'
  ));

	while($children->have_posts()) {
		$children->the_post();
	?>
		<li>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<span class="link-description"><?php echo get_field("description"); ?></span>
		</li>
	<?php } ?>
</ul><!-- /.subpages -->

<?php wp_reset_postdata(); ?>
</aside> <!-- /.menu-widget -->

<?php
$post_object = get_field('choose_your_cta');
if( $post_object ):

// override $post
$post = $post_object;
setup_postdata( $post );
?>

<aside class="widget text-widget">
<div class="sidebar-testimonial call-to-action">
    <div>
      <?php the_post_thumbnail(); ?>
    	<?php the_content(); ?>
    </div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
</div><!-- /.sidebar-testimonial -->
</aside><!-- /.widget .text-widget -->
<?php endif; ?>
