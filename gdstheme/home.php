<?php get_header(); ?>

<?php get_template_part('inc/modules/content', 'title'); ?>

<div class="content-container">
  <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
  <div class="row breadcrumb-row">
    <div class="medium-12 columns">
      <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
    </div>
  </div>
  <?php } ?>
  <div class="row title-row">
    <div class="large-12 columns">
      <?php
        // This WP_Query gets the actual page content for this page and loads
        // it into the page because this template is used for the posts page
        // which does not let you use the usual the_content() call to load content
        $query = new WP_Query( array(
          'p' => get_option( 'page_for_posts'),
          'post_type' => 'any'
        ));
        while ($query->have_posts()) {
          $query->the_post();
          the_content();
        };
        wp_reset_postdata();
      ?>
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
      <div class="feed">
        <div class="row small-up-1 medium-up-2 large-up-3" data-equalizer data-equalize-by-row="true">
          <?php while ( have_posts() ) : the_post(); ?>
            <div class="column">
              <div class="callout" data-equalizer-watch>
                <div class="row">
                  <div class="large-12 columns">
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(300, 220) ); ?></a>
                  </div>
                </div>
                <div class="description">
                  <div class="row">
                    <div class="large-12 columns">
                      <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    </div>
                  </div>
                  <div class="row date-row">
                    <div class="large-12 columns">
                      <small class="text-center"><?php the_time('F j, Y'); ?></small>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                      <p><?php echo excerpt(20); ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
</div>



<?php get_footer(); ?>
