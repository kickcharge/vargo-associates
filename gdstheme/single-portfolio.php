    <?php get_header(); ?>
    
    <section id="portfolio-container">

		<?php 
			$images = get_field('photo_gallery');
			if( $images ):
		?>
		
		    <div class="photo-large">
		        <?php foreach( $images as $image ): ?>
					<?php 
						if (get_field('image_positioning', $image['id']) == 'left_top') {
							$bg_position = "left top";
						} else if (get_field('image_positioning', $image['id']) == 'left_center') {
							$bg_position = "left center";
						} else if (get_field('image_positioning', $image['id']) == 'left_bottom') {
							$bg_position = "left bottom";
						} else if (get_field('image_positioning', $image['id']) == 'right_top') {
							$bg_position = "right top";
						} else if (get_field('image_positioning', $image['id']) == 'right_center') {
							$bg_position = "right center";
						} else if (get_field('image_positioning', $image['id']) == 'right_bottom') {
							$bg_position = "right bottom";
						} else if (get_field('image_positioning', $image['id']) == 'center_top') {
							$bg_position = "center top";
						} else if (get_field('image_positioning', $image['id']) == 'center_center') {
							$bg_position = "center center";
						} else if (get_field('image_positioning', $image['id']) == 'center_bottom') {
							$bg_position = "center bottom";
						} else if (get_field('image_positioning', $image['id']) == '') {
							$bg_position = "center center";
						}
					?>
    		        <?php if(get_field('featured_portfolio_image', $image['id']) ) { ?>
    		        
    		            <div
    		            	class="photo"
	    		            style="
	    		            	position: relative;
		    		            background-image: url(<?php echo $image['url']; ?>);
								background-position: <?php echo $bg_position; ?>;
								background-repeat: no-repeat;
				    		    background-size: cover;
				    		">
    		            </div> <!-- /.photo -->
    		        
    		        <?php } else { ?>
    		        
    		            <!-- Do Nothing -->
    		        
    		        <?php } ?>
    		        
		        <?php endforeach; ?>
		    </div><!-- /.photo-large -->
		
		<?php endif; ?>

    </section><!-- /#portfolio-container -->

	<section class="container content_lightbox">

        <div class="wrap">

          <article id="content">

		  	<?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

			  	<h2 class="portfolio-title"><?php the_title(); ?></h2>

			  	<?php the_content()?>

            <?php } ?>

            <?php } else { ?>

              <h2>No content found.</h2>

            <?php } ?>

			<div class="content_gallery">

				<?php
					$images = get_field('photo_gallery');
					if( $images ):
				?>
				    <ul class="portfolio-thumbs">
				        <?php foreach( $images as $image ): ?>
				            <li>
				                <a title="<?php echo $image['caption']; ?>" rel="group" class="fancybox" href="<?php echo $image['url']; ?>">
				                     <img title="<?php echo $image['caption']; ?>" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
				                </a>
				            </li>
				        <?php endforeach; ?>
				    </ul><!-- /.portfolio-thumbs -->
				<?php endif; ?>
				
				<h3 class="portfolio-link"><a href="<?php echo get_post_type_archive_link( 'portfolio' ); ?>">More Galleries</a></h3>

			</div><!-- /.content_gallery -->

          </article><!-- /#content -->

          <?php get_sidebar( 'right' ); ?>

        </div> <!-- /.wrap -->
      </section> <!-- /.container -->

      <?php get_footer(); ?>