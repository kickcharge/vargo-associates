<?php
/*
Template Name: Full Width
*/

get_header(); ?>

      <?php get_template_part('inc/modules/content', 'title'); ?>
      <div class="content-container">
        <?php if(get_field('breadcrumbs_positioning', 'option') == 'content' && function_exists('yoast_breadcrumb') ) { ?>
        <div class="row breadcrumb-row">
          <div class="medium-12 columns">
            <?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
          </div>
        </div>
        <?php } ?>
  			<div class="row">
  	      <div class="medium-12 columns">
  					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  				  		<?php the_content(); ?>
  		        <?php endwhile; ?>
  		      <?php else : ?>
  				  	<h2>Sorry Nothing Found</h2>
  		      <?php endif; ?>
  	      </div>>
          <?php get_template_part('inc/acf/page', 'builder'); ?>
        </div>
      </div>

<?php get_footer(); ?>
