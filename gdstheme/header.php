<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>
    </head>

  <body <?php body_class(); ?>>
  <div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
      <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
        <?php
          $main_menu = get_field( 'main_menu_selector', 'option' );
          $defaults = array(
            'theme_location'  => 'primary_menu',
            'menu'            => $main_menu,
            'container'       => 'div',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'vertical menu',
            'menu_id'         => 'main_menu_offcanvas',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="vertical menu %2$s">%3$s</ul>',
            'depth'           => 0,
            'walker' 		  => new GDS_Walker()
          );

          wp_nav_menu( $defaults );

        ?>
      </div>
      <div class="off-canvas-content" data-off-canvas-content>
          <div class="header hide-for-print" data-sticky-container>
            <header id="pageHeader" data-sticky data-margin-top="0" data-top-anchor="pageHeader:bottom" data-sticky-on="large">
              <div class="row">
                <div class="header-icon-container small-3 columns hide-for-large">
                  <a href="tel:<?php the_field('header_message', 'option'); ?>"><i class="fa fa-phone"></i></a>
                </div>
                <div class="large-3 small-6 columns" id="logo">
                  <a href="<?php bloginfo('url'); ?>">
            			<?php
            				$image = get_field('company_logo', 'option');
            				if( !empty($image) ): ?>
            				<img src="<?php echo $image['url']; ?>" class="float-center" alt="<?php echo $image['alt']; ?>" />
            			<?php endif; ?>
                  </a>
                </div>
                <div class="header-icon-container small-3 columns hide-for-large">
                  <a href="#" data-toggle="offCanvas"><i class="fa fa-bars"></i></a>
                </div>
                <div class="large-9 show-for-large columns">
                  <div class="row header-message-row">
                    <div class="large-12 columns">
                      <span class="float-right">
                        <?php the_field('header_message', 'option'); ?>
                        <div class="social">
                          <div class="wrap">
                            <?php if(get_field('social_icon_list', 'option') ) : ?>
                              <?php the_field('social_icon_list', 'option'); ?>
                          <?php endif; ?>
                          </div> <!-- /.wrap -->
                        </div> <!-- /.social -->
                      </span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                      <?php
              					$main_menu = get_field( 'main_menu_selector', 'option' );
              					$defaults = array(
              						'theme_location'  => 'primary_menu',
              						'menu'            => $main_menu,
              						'container'       => 'div',
              						'container_class' => '',
              						'container_id'    => '',
              						'menu_class'      => 'dropdown menu float-right',
              						'menu_id'         => 'main_menu',
              						'echo'            => true,
              						'fallback_cb'     => 'wp_page_menu',
              						'before'          => '',
              						'after'           => '',
              						'link_before'     => '',
              						'link_after'      => '',
              						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              						'depth'           => 0,
              						'walker' 		  => new GDS_Walker()
              					);

              					wp_nav_menu( $defaults );

              				?>
                    </div>
                  </div>
                </div>
              </div>
            </header>
          </div> <!-- /.header -->
