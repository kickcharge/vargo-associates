      <?php get_header(); ?>

      <section class="banner">
	  	<div class="wrap">
            <?php
            	$blog_page_id = get_option('page_for_posts');
            	echo '<p>'.get_page($blog_page_id)->post_title.'</p>';
            ?>
            <div class="breadcrumbs">
            <ul>
              <?php if(function_exists('bcn_display'))
              {
                  bcn_display_list();
              }?>
            </ul>
          </div> <!-- /.breadcrumbs -->
	  	</div><!-- /.wrap -->
      </section> <!-- /.banner -->

      <section class="container">
        <div class="wrap">

          <?php if ( have_posts() ) : ?>

          <div class="feed">

            <div class="columns">

          <?php while ( have_posts() ) : the_post(); ?>

              <div class="col-3">
                <article class="post">

                  <div class="featured-img">
                      <div class="resource-tile-mask">
                        <div class="yuck-Y">
                          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                          <p><?php the_category(', '); ?></p>
                          <div class="icon icon-expand">Expand</div>
                        </div>
                      </div>
                      <?php

                        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                              the_post_thumbnail('featured-thumb');
                            } else {
                              echo '<img src="'.get_template_directory_uri().'/images/bg/bg-seeds.gif" alt="No Thumbnail" width="292" height="220">';
                            }

                      ?>
                  </div> <!-- /.featured-img -->

                  <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                  <div class="post-info">
                    <div class="date"><?php the_time('j F') ?></div> <!-- /.date -->                   
                  </div> <!-- /.post-info -->

                  <p class="post-excerpt">
	                  <?php
					  	$excerpt = get_the_excerpt();
					  	echo word_limited_excerpt($excerpt,12);
                  	?>...
                  </p> <!-- /.post-excerpt -->

                </article> <!-- /.post -->
              </div> <!-- /.col-3 -->

            <?php endwhile; ?>

            </div> <!-- /.columns -->

        </div> <!-- /.feed -->

        <?php else : ?>

          <h1 class="no-content">No content found.</h1>

        <?php endif; ?>

        </div> <!-- /.wrap -->
      </section> <!-- /.container -->

      <?php get_footer(); ?>