<?php
//////////////////////////////////////////////////////////////////
// Custom Functions.php file for our themes
// Add / Enable WP functions and filters for our site
// Version: 3.0
// WP Test Version: 4.2.2
// Creation Date: 5/12/15
// Author: Graphic D-Signs
//////////////////////////////////////////////////////////////////

// This is required and will need to be changed with every site.
if ( ! isset( $content_width ) ) $content_width = 1200; // This variable is a global variable that is used to set the width of the content on the site.
// End of content width

// Register styles and scripts in WP
function mytheme_enqueue_scripts() {

	// Register Styles
	wp_register_style('print_style', get_template_directory_uri() . '/print.css', array(), 1.0, 'print');
	wp_register_style('theme_style', get_template_directory_uri() . '/style.css', array(), 1.0, 'screen');


	// Enqueue Styles/Scripts
   	wp_enqueue_style('print_style');
   	wp_enqueue_style('theme_style');

	wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'mytheme_enqueue_scripts', 1);

// Change this to your theme text domain, used for internationalising strings
$theme_text_domain = 'gdstheme';

/**
 * Array of configuration settings. Amend each line as needed.
 * If you want the default strings to be available under your own theme domain,
 * leave the strings uncommented.
 * Some of the strings are added into a sprintf, so see the comments at the
 * end of each line for what each argument will be.
 */
$config = array(
    'domain'            => $theme_text_domain,           // Text domain - likely want to be the same as your theme.
    'default_path'      => '',                           // Default absolute path to pre-packaged plugins
    'parent_menu_slug'  => 'themes.php',         // Default parent menu slug
    'parent_url_slug'   => 'themes.php',         // Default parent URL slug
    'menu'              => 'install-required-plugins',   // Menu slug
    'has_notices'       => true,                         // Show admin notices or not
    'is_automatic'      => false,            // Automatically activate plugins after installation or not
    'message'           => '',               // Message to output right before the plugins table
    'strings'           => array(
        'page_title'                                => __( 'Install Required Plugins', $theme_text_domain ),
        'menu_title'                                => __( 'Install Plugins', $theme_text_domain ),
        'installing'                                => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
        'oops'                                      => __( 'Something went wrong with the plugin API.', $theme_text_domain ),
        'notice_can_install_required'               => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
        'notice_can_install_recommended'            => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
        'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
        'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
        'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
        'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
        'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
        'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
        'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
        'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
        'return'                                    => __( 'Return to Required Plugins Installer', $theme_text_domain ),
        'plugin_activated'                          => __( 'Plugin activated successfully.', $theme_text_domain ),
        'complete'                                  => __( 'All plugins installed and activated successfully. %s', $theme_text_domain ) // %1$s = dashboard link
    )
);

// add a favicon to your
function blog_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
	echo '<link rel="apple-touch-icon" href="'.get_bloginfo('wpurl').'/apple-touch-icon.png" />';
	echo '<link rel="apple-touch-icon" sizes="72x72" href="'.get_bloginfo('wpurl').'/apple-touch-icon-72x72.png" />';
	echo '<link rel="apple-touch-icon" sizes="114x114" href="'.get_bloginfo('wpurl').'/apple-touch-icon-114x114.png" />';

}
add_action('wp_head', 'blog_favicon');

//////////////////////////////////////////////////////////////////
// Load ACF fields through Yoast 3.0+ - New JS method
//////////////////////////////////////////////////////////////////

function load_yoast_acf_link() {
	wp_enqueue_script( 'acf_yoastseo', get_template_directory_uri() . '/inc/admin/acf_yoastseo.js', 'jquery' );
}

add_action( 'admin_init', 'load_yoast_acf_link' );

//////////////////////////////////////////////////////////////////
// Mixed Content SSL Fix
//////////////////////////////////////////////////////////////////

add_filter('script_loader_src', 'agnostic_script_loader_src', 20,2);
function agnostic_script_loader_src($src, $handle) {
	return preg_replace('/^(http|https):/', '', $src);
}

//////////////////////////////////////////////////////////////////
// Remove query strings from CSS/JS files - SEO Purposes
//////////////////////////////////////////////////////////////////

function qsr_remove_script_version( $src ){
    $parts = explode( '?ver', $src );
        return $parts[0];
}
add_filter( 'script_loader_src', 'qsr_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'qsr_remove_script_version', 15, 1 );

//////////////////////////////////////////////////////////////////
// WordPress title tag update: https://codex.wordpress.org/Title_Tag
//////////////////////////////////////////////////////////////////

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );
function title_filter_function($title) {
	return get_bloginfo('name') . ' | ' . $title;
}
add_filter( 'wpseo_title', 'title_filter_function', 10, 1 );

//////////////////////////////////////////////////////////////////
// Register menus
//////////////////////////////////////////////////////////////////

register_nav_menus( array(
	'primary_menu' => 'Primary Menu',
	'sidebar_menu' => 'Sidebar Menu'
));

// Create custom menu walker class that adds better class names to menu/sub-menus
class GDS_Walker extends Walker_Nav_Menu {

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
        $GLOBALS['dd_children'] = ( isset($children_elements[$element->ID]) )? 1:0;
        $GLOBALS['dd_depth'] = (int) $depth;
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

  function start_lvl(&$output, $depth) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"menu level-".$depth."\">\n";
  }
}

add_filter('nav_menu_css_class','add_parent_css',10,2);
function  add_parent_css($classes, $item){
     global  $dd_depth, $dd_children;
     $classes[] = 'depth'.$dd_depth;
     if($dd_children)
         $classes[] = 'parent';
    return $classes;
}

//Add class to parent pages to show they have subpages (only for automatic wp_nav_menu)

function add_parent_class( $css_class, $page, $depth, $args ) {
   if ( ! empty( $args['has_children'] ) )
       $css_class[] = 'parent';
   return $css_class;
}
add_filter( 'page_css_class', 'add_parent_class', 10, 4 );

	//////////////////////////////////////////////////////////////////
	// Function to get category ID (grandparent, parent)
	// Used on sidebar for Products CPT.
	//////////////////////////////////////////////////////////////////

    /**
    * Returns ID of top-level parent category, or current category if you are viewing a top-level
    *
    * @param	string		$catid 		Category ID to be checked
    * @return 	string		$catParent	ID of top-level parent category
    */

    function pa_category_top_parent_id ($catid) {

     while ($catid) {
      $cat = get_category($catid); // get the object for the catid
      $catid = $cat->category_parent; // assign parent ID (if exists) to $catid
      // the while loop will continue whilst there is a $catid
      // when there is no longer a parent $catid will be NULL so we can assign our $catParent
      $catParent = $cat->cat_ID;
     }

    return $catParent;
    }

//////////////////////////////////////////////////////////////////
// Adjust the body class
//////////////////////////////////////////////////////////////////

/*
	Version: 2.2

*/
if( !class_exists('Awebsome_Browser_Selector') ):
/**
 * Awebsome Browser Selector
 *
 * @since 2.0
 */
class Awebsome_Browser_Selector
{
	/**
	 * PHP5 Constructor
	 */
	public function __construct()
	{
		add_filter('body_class', array(&$this, 'add_body_classes'));

		// BuddyPress support
		add_action('bp_include', array(&$this, 'bp_activate'));
	}

	/**
	 * Only load code that needs BuddyPress to run once BP is loaded and initialized
	 *
	 * @since 2.0
	 */
	public function bp_activate()
	{
		require(dirname(__FILE__) .'/'. __FILE__);
	}

	/**
	 * Parses the user agent string into browser, version and platform
	 *
	 * @author Jesse G. Donat <donatj@gmail.com>
	 * @link https://github.com/donatj/PhpUserAgent
	 * @param string $ua
	 * @return array an array with browser, version and platform keys
	 *
	 * @since 2.0
	 */
	public function parse_UA_string($ua = null)
	{
		if( is_null($ua) ) $ua = $_SERVER['HTTP_USER_AGENT'];

		$data = array(
			'platform' => '',
			'browser'  => '',
			'version'  => '',
		);

		if( preg_match('/\((.*?)\)/im', $ua, $regs) )
		{
			/*
			(?P<platform>Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows\ Phone\ OS|Windows|Silk|linux-gnu|BlackBerry|Xbox)
			(?:\ [^;]*)?
			(?:;|$)
			*/
			preg_match_all('/(?P<platform>Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows\ Phone\ OS|Windows|Silk|linux-gnu|BlackBerry|Nintendo\ Wii|Xbox)(?:\ [^;]*)?(?:;|$)/imx', $regs[1], $result, PREG_PATTERN_ORDER);

			$priority = array('Android', 'Xbox');

			$result['platform'] = array_unique($result['platform']);

			if( count($result['platform']) > 1 )
			{
				if( $keys = array_intersect($priority, $result['platform']) ) $data['platform'] = reset($keys);
				else $data['platform'] = $result['platform'][0];
			}
			elseif( isset($result['platform'][0]) ) $data['platform'] = $result['platform'][0];
		}

		if( $data['platform'] == 'linux-gnu' ) $data['platform'] = 'Linux';
		if( $data['platform'] == 'CrOS' ) $data['platform'] = 'Chrome OS';

		/*
		(?<browser>Camino|Kindle|Kindle\ Fire\ Build|Firefox|Safari|MSIE|AppleWebKit|Chrome|IEMobile|Opera|Silk|Lynx|Version|Wget|curl|PLAYSTATION\ \d+)
		(?:;?)
		(?:(?:[/\ ])(?<version>[0-9.]+)|/(?:[A-Z]*))
		*/
		preg_match_all('%(?P<browser>Camino|Kindle|Kindle\ Fire\ Build|Firefox|Safari|MSIE|AppleWebKit|Chrome|IEMobile|Opera|Silk|Lynx|Version|Wget|curl|PLAYSTATION\ \d+)(?:;?)(?:(?:[/ ])(?P<version>[0-9.]+)|/(?:[A-Z]*))%x',
			$ua, $result, PREG_PATTERN_ORDER);

		$key = 0;

		$data['browser'] = $result['browser'][0];
		$data['version'] = $result['version'][0];

		if( ($key = array_search('Kindle Fire Build', $result['browser'])) !== false || ($key = array_search('Silk', $result['browser'])) !== false )
		{
			$data['browser']  = $result['browser'][$key] == 'Silk' ? 'Silk' : 'Kindle';
			$data['platform'] = 'Kindle Fire';

			if( !($data['version']  = $result['version'][$key]) ) $data['version'] = $result['version'][array_search( 'Version', $result['browser'] )];
		}
		elseif( ($key = array_search('Kindle', $result['browser'])) !== false )
		{
			$data['browser']  = $result['browser'][$key];
			$data['platform'] = 'Kindle';
			$data['version']  = $result['version'][$key];
		}
		elseif( $result['browser'][0] == 'AppleWebKit' )
		{
			if( ( $data['platform'] == 'Android' && !($key = 0) ) || $key = array_search('Chrome', $result['browser']) )
			{
				$data['browser'] = 'Chrome';

				if( ($vkey = array_search('Version', $result['browser'])) !== false ) $key = $vkey;
			}
			elseif( $data['platform'] == 'BlackBerry' )
			{
				$data['browser'] = 'BlackBerry Browser';

				if( ($vkey = array_search('Version', $result['browser'])) !== false ) $key = $vkey;
			}
			elseif( $key = array_search('Safari', $result['browser']) )
			{
				$data['browser'] = 'Safari';

				if( ($vkey = array_search('Version', $result['browser'])) !== false ) $key = $vkey;
			}

			$data['version'] = $result['version'][$key];
		}
		elseif( ($key = array_search('Opera', $result['browser'])) !== false )
		{
			$data['browser'] = $result['browser'][$key];
			$data['version'] = $result['version'][$key];

			if( ($key = array_search('Version', $result['browser'])) !== false ) $data['version'] = $result['version'][$key];
		}
		elseif( $result['browser'][0] == 'MSIE' )
		{
			if( $key = array_search('IEMobile', $result['browser']) ) $data['browser'] = 'IEMobile';
			else
			{
				$data['browser'] = 'MSIE';
				$key = 0;
			}

			$data['version'] = $result['version'][$key];
		}
		elseif( $key = array_search('PLAYSTATION 3', $result['browser']) !== false )
		{
			$data['platform'] = 'PLAYSTATION 3';
			$data['browser']  = 'NetFront';
		}

		return $data;
	}

	/**
	 * Converts the parsed User Agent string to CSS classes
	 *
	 * @param $data array Server User Agent parsed
	 * @return string     Parsed CSS classes (platform + browser + version)
	 *
	 * @since 2.0
	 */
	public function parse_UA_to_classes($data)
	{
		$css['platform'] = self::filter_platform($data['platform']);
		$css['browser']  = self::filter_browser($data['browser']);
		$css['version']  = self::filter_version($data['version']);

		return join(' ', $css);
	}

	/**
	 * Filters the Platform CSS string
	 *
	 * @param $platform string Server User Agent Platform parsed
	 * @return string          CSS Platform class
	 *
	 * @since 2.0
	 */
	public function filter_platform($platform)
	{
		$p = '';

		# Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows\ Phone\ OS|Windows|Silk|linux-gnu|BlackBerry|Xbox
		switch($platform)
		{
			// desktop
			case 'Windows'   : $p = 'win';  break;
			case 'Linux'     : $p = 'lnx';  break;
			case 'Macintosh' : $p = 'mac';  break;
			case 'ChromeOS'  : $p = 'cros'; break;

			// mobile
			case 'Android'          : $p = 'android';    break;
			case 'iPhone'           : $p = 'iphone';     break;
			case 'iPad'             : $p = 'ipad';       break;
			case 'Windows Phone OS' : $p = 'winphone';   break;
			case 'Kindle'           : $p = 'kindle';     break;
			case 'Kindle Fire'      : $p = 'kindlefire'; break;
			case 'BlackBerry'       : $p = 'blackberry'; break;

			// consoles
			case 'Xbox'          : $p = 'xbox'; break;
			case 'PLAYSTATION 3' : $p = 'ps3';  break;
			case 'Nintendo Wii'  : $p = 'wii'; break;

			default : break;
		}

		return $p;
	}

	/**
	 * Filters the Browser CSS string
	 *
	 * @param $browser string Server User Agent Browser parsed
	 * @return string         CSS Browser class
	 *
	 * @since 2.0
	 */
	public function filter_browser($browser)
	{
		$b = '';

		# Camino|Kindle|Kindle\ Fire\ Build|Firefox|Safari|MSIE|AppleWebKit|Chrome|IEMobile|Opera|Silk|Lynx|Version|Wget|curl|PLAYSTATION\
		switch($browser)
		{
			case 'Camino'            : $b = 'camino';   break;
			case 'Kindle'            : $b = 'kindle';   break;
			case 'Firefox'           : $b = 'firefox';  break;
			case 'Safari'            : $b = 'safari';   break;
			case 'Internet Explorer' : $b = 'ie';       break;
			case 'IEMobile'          : $b = 'iemobile'; break;
			case 'Chrome'            : $b = 'chrome';   break;
			case 'Opera'             : $b = 'opera';    break;
			case 'Silk'              : $b = 'silk';     break;
			case 'Lynx'              : $b = 'lynx';     break;
			case 'Wget'              : $b = 'wget';     break;
			case 'Curl'              : $b = 'curl';     break;

			default : break;
		}

		return $b;
	}

	/**
	 * Filters the Version CSS string
	 *
	 * @param $browser string Server User Agent Version parsed
	 * @return string         CSS Version class
	 *
	 * @since 2.0
	 */
	public function filter_version($version)
	{
		$v = explode('.', $version);

		return !empty($v[0]) ? 'v'. $v[0] : '';
	}

	/**
	 * Callback function for the body_class filter
	 *
	 * @param $classes array Body tag classes
	 * @return array         Body tag classes + parsed UA classes
	 *
	 * @since 2.0
	 */
	function add_body_classes($classes)
	{
		$classes[] = self::parse_UA_to_classes( self::parse_UA_string() );

		return $classes;
	}
} // class
endif;

/**
 * Enable the plugin
 */
new Awebsome_Browser_Selector;

if(!function_exists('get_post_top_ancestor_id')){
/**
 * Gets the id of the topmost ancestor of the current page. Returns the current
 * page's id if there is no parent.
 *
 * @uses object $post
 * @return int
 */
function get_post_top_ancestor_id(){
    global $post;

    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }

    return $post->ID;
}}

//////////////////////////////////////////////////////////////////
// Media Functions
//////////////////////////////////////////////////////////////////

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'mini-gallery', 341, 234, true );
	add_image_size( 'mini-gallery-full', 682, 468, true );
	add_image_size( 'featured-thumb', 300, 220, true );
	add_image_size( 'featured-full', 641, 426, true );
	add_image_size( 'home-gallery', 549, 256, true ); // Album View Portfolio
}

function add_webm_mime_for_upload($mimes) {
	$mimes = array_merge($mimes, array(
		'webm' => 'video/webm',
		'mp4' => 'video/mp4',
		'ogv' => 'video/ogg',
		'svg' => 'image/svg+xml',
		'pdf' => 'application/pdf'
	));
	return $mimes;
}
add_filter('upload_mimes', 'add_webm_mime_for_upload');

// Increase Image quality to 100%
function gpp_jpeg_quality_callback($arg) {
	return (int)100;
}
add_filter('jpeg_quality', 'gpp_jpeg_quality_callback');

//////////////////////////////////////////////////////////////////
// Add Theme Support for WP Actions
//////////////////////////////////////////////////////////////////

add_filter('the_content', 'do_shortcode', 11);
add_theme_support( 'html5', array( 'search-form' ) );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Highlight search terms in the excerpt on search.php
function search_excerpt_highlight() {
    $excerpt = get_the_excerpt();
    $keys = implode('|', explode(' ', get_search_query()));
    $excerpt = preg_replace('/(' . $keys .')/iu', '<strong class="search-highlight">\0</strong>', $excerpt);

    echo '<p>' . $excerpt . '</p>';
}
add_filter('highlight_excerpt', 'search_excerpt_highlight');

// Add support to set custom excerpt lengths in your theme
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
add_filter( 'excerpt', 'new_excerpt_more' );

if(!function_exists('get_post_top_ancestor_id')){
/**
 * Gets the id of the topmost ancestor of the current page. Returns the current
 * page's id if there is no parent.
 *
 * @uses object $post
 * @return int
 */
function get_post_top_ancestor_id(){
    global $post;

    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }

    return $post->ID;
}}


///////////////////////////////////////////////////////////////////
// Create new condition is_tree to detect child pages
//////////////////////////////////////////////////////////////////

function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;         // load details about this page
	if(is_page()&&($post->post_parent==$pid||is_page($pid)))
               return true;   // we're at the page or at a sub page
	else
               return false;  // we're elsewhere
};

//////////////////////////////////////////////////////////////////
// Search Form shortcode
//////////////////////////////////////////////////////////////////

add_shortcode('search_form', 'rlv_search_form');
function rlv_search_form() {
	$url = get_site_url();
	$form = <<<EOH
	<form role="search" method="get" id="searchform" class="searchform" action="$url">
		<div class="input-group">
			<input type="text" value="" name="s" id="s" class="input-group-field" placeholder="Search" />
			<div class="input-group-button">
				<button class="button" type="submit"><i class="fa fa-search"></i></button>
			</div>
		</div>
	</form>
EOH;
return $form;
}

//////////////////////////////////////////////////////////////////
// ACF Custom Functions
//////////////////////////////////////////////////////////////////

//Register Options page & Add Custom Sub-Sections
add_action('admin_menu', 'add_options_pages');
function add_options_pages() {
  if (!function_exists('acf_add_options_page')) {
    return;
  }
  acf_add_options_page(array('page_title' => 'Theme Options'));
  // the following parent url will be auto generated by the above call
  // you can change the slug by specifying a slug for the page
  $parent = 'acf-options-theme-options';
  $sub_options_pages = array('Global Styles', 'Header', 'Sidebar', 'Footer');
  foreach ($sub_options_pages as $title) {
    acf_add_options_sub_page(array(
    	'title' => $title,
    	'parent' => $parent
    ));
  }
}

// Hide ACF Fields Menu for non-admins
add_filter('acf/settings/show_admin', 'my_acf_show_admin');

function my_acf_show_admin( $show ) {

    return current_user_can('manage_options');

}

// Move the Wordpress Content area within ACF Tab
add_action('acf/input/admin_head', 'my_acf_admin_head');

function my_acf_admin_head() {

    ?>
    <script type="text/javascript">
    (function($) {

        $(document).ready(function(){
            $('.acf-field-5554e834ab9f3 .acf-input').append( $('#postdivrich') );
        });

    })(jQuery);
    </script>
    <style type="text/css">
        .acf-field #wp-content-editor-tools {
            background: transparent;
            padding-top: 0;
        }
        #provideddiv.postbox {
	        background: transparent;
	        padding-top: 0;
        }
    </style>
    <?php

}

//define( 'YOUR_API_KEY', 'AIzaSyCmUt8xoUkA6KtCxfmpoNgXIrLPztlKZSw' );

//Load ACF Fields

include_once('inc/acf/case-studies.php');
include_once('inc/acf/footer-options.php');
include_once('inc/acf/general-options.php');
include_once('inc/acf/header-options.php');
include_once('inc/acf/home-page-features.php');
include_once('inc/acf/portfolio.php');
include_once('inc/acf/products.php');
include_once('inc/acf/sidebar-options.php');
include_once('inc/acf/coupon-options.php');
include_once('inc/acf/custom-content.php');
include_once('inc/acf/image-attributes.php');
include_once('inc/acf/team-bio.php');
include_once('inc/acf/category-image.php');

//////////////////////////////////////////////////////////////////
// Gravity Forms Customizations
//////////////////////////////////////////////////////////////////

// Give the ability to split fields in to multiple columns
function gform_column_splits($content, $field, $value, $lead_id, $form_id) {
	if(!IS_ADMIN) { // only perform on the front end

		// target section breaks
		if($field['type'] == 'section') {
			$form = RGFormsModel::get_form_meta($form_id, true);

			// check for the presence of our special multi-column form class
			$form_class = explode(' ', $form['cssClass']);
			$form_class_matches = array_intersect($form_class, array('two-column'));

			// check for the presence of our special section break column class
			$field_class = explode(' ', $field['cssClass']);
			$field_class_matches = array_intersect($field_class, array('gform_column'));

			// if we have a column break field in a multi-column form, perform the list split
			if(!empty($form_class_matches) && !empty($field_class_matches)) {

				// we'll need to retrieve the form's properties for consistency
				$form = RGFormsModel::add_default_properties($form);
				$description_class = rgar($form, 'descriptionPlacement') == 'above' ? 'description_above' : 'description_below';

				// close current field's li and ul and begin a new list with the same form properties
				return '</li></ul><ul class="gform_fields '.$form['labelPlacement'].' '.$description_class.' '.$field['cssClass'].'"><li class="gfield gsection">';

			}
		}
	}

	return $content;
}
add_filter('gform_field_content', 'gform_column_splits', 10, 5);

// Fix onsubmit issue with validation erros where the error message is blocked by floating nav
add_filter("gform_confirmation_anchor", create_function("","return true;"));

// Fix tab index issue with multiple forms on 1 page
add_filter( 'gform_tabindex', 'gform_tabindexer', 10, 2 );
function gform_tabindexer( $tab_index, $form = false ) {
    $starting_index = 1000; // if you need a higher tabindex, update this number
    if( $form )
        add_filter( 'gform_tabindex_' . $form['id'], 'gform_tabindexer' );
    return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
}

// Fix issue where on multi-page Gravity Form the page would jump to the bottom when clicking next.
add_filter("gform_confirmation_anchor",
create_function("","return false;"));

// Turn off CSS (we use SCSS for this)
add_filter( 'pre_option_rg_gforms_disable_css', '__return_true' );

//////////////////////////////////////////////////////////////////
// Register Widget Areas
//////////////////////////////////////////////////////////////////

if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'Sidebar 1',
		'id' => 'sidebar1',
		'before_widget' => '<article id="%1$s" class="widget %2$s">',
		'after_widget' => '</article>',
		'before_title' => '<h6 class="offscreen">',
		'after_title' => '</h6>',
	));

	register_sidebar(array(
		'name'=> 'Sidebar 2',
		'id' => 'sidebar2',
		'before_widget' => '<article id="%1$s" class="widget %2$s">',
		'after_widget' => '</article>',
		'before_title' => '<h6 class="offscreen">',
		'after_title' => '</h6>',
	));

	register_sidebar(array(
		'name'=> 'Footer 1',
		'id' => 'footersidebar1',
		'before_widget' => ' ',
		'after_widget' => ' ',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));

}

//////////////////////////////////////////////////////////////////
// Get Post Thumbnail Full Url
//////////////////////////////////////////////////////////////////

function getPostThumbUrl($post_id, $thumb_size = 'full') {
	$thumb_id = get_post_thumbnail_id($post_id);
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, $thumb_size, true);
	$thumb_url = $thumb_url_array[0];
	return $thumb_url;
}

//////////////////////////////////////////////////////////////////
// Get Company Logo
//////////////////////////////////////////////////////////////////

function getCompanyLogo() {

	$imageID = get_field('company_logo', 'option');

	if(!empty($imageID)):
		$image = wp_get_attachment_image_src($imageID, 'full');
	endif;

	return $image[0];

}

//////////////////////////////////////////////////////////////////
// Get Header Image
//////////////////////////////////////////////////////////////////

function headerImage() {
        global $post;
        $parents = get_post_ancestors( $post->ID );
        $id = ($parents) ? $parents[count($parents)-1]: $post->ID;
        if(has_post_thumbnail( $id )) {
                $postThumb = get_the_post_thumbnail( $id, 'post-thumbnail');
        }
        return $postThumb;
}

//////////////////////////////////////////////////////////////////
// Add Custom Shortcodes / CPTs / Theme Dependencies
//////////////////////////////////////////////////////////////////

require_once(get_template_directory() . '/inc/dependencies/required-plugins.php');
include_once('inc/shortcodes/shortcodes.php');

// Conditionally enable CPTs based on options checked within the General Options
/* Checks to see if the acf pro plugin is activated  */
if ( ! function_exists( 'is_plugin_active' ) ) {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}
if ( is_plugin_active('advanced-custom-fields-pro/acf.php') )  {

	$cpt_options = get_field('custom_section', 'option');

	if( is_array($cpt_options) ) {

		if(in_array('case_studies', $cpt_options)) {
			include_once('inc/cpt/cpt_case-studies.php');
			include_once('inc/cpt/tax_services-provided.php');

		} if(in_array('portfolio', $cpt_options)) {
			include_once('inc/cpt/cpt_portfolio.php');

		} if(in_array('testimonials', $cpt_options)) {
			include_once('inc/cpt/cpt_testimonials.php');

		} if(in_array('products', $cpt_options)) {
			include_once('inc/cpt/cpt_products.php');

		} if(in_array('coupons', $cpt_options)) {
			include_once('inc/cpt/cpt_coupons.php');

		} if(in_array('ctas', $cpt_options)) {
			include_once('inc/cpt/cpt_ctas.php');
			include_once('inc/acf/cta-options.php');
		}

		if(in_array('team', $cpt_options)) {
			include_once('inc/cpt/cpt_team.php');
			include_once('inc/acf/team-bio.php');
		}

	} // is_array

}

//include_once('inc/cpt/cpt_import.php');
//include_once('inc/licensing/license.php');

?>
